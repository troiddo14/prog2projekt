#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "header.h"

char kvar[150];
int cijena;

FILE* otvaranjeFile() {
	FILE* fp = NULL;
	fp = fopen("korisnici.txt", "a");
	if (fp == NULL) {
		printf("Datoteku nije moguce otvoriti.");
		return fp;
	}
	return fp;
}

KORISNIK unos(FILE* fp) {
	/*char ime[20];
	char prezime[20];
	int kontakt[20];
	char mail[50];
	*/
	KORISNIK temp = { 0 };
	KORISNIK* poks = &temp;
	printf("Unesite ime: ");
	scanf("%s", poks->ime);
	fprintf(fp, " %s", poks->ime);


	printf("Unesite prezime: ");
	scanf("%s", poks->prezime);
	fprintf(fp, " %s", poks->prezime);


	printf("Unesite kontakt: ");
	scanf("%s", poks->kontakt);
	fprintf(fp, " %s ", poks->kontakt);


	printf("Unesite mail: ");
	scanf("%s", poks->mail);
	fprintf(fp, " %s", poks->mail);
	return temp;
}

int model1(FILE* fp) {
	char n1[] = "A10-A41s";
	char n2[] = "A41-A80";
	char n3[] = "S6-S9";
	char n4[] = "S9+ - S20+";
	char n5[] = "J5-J7";
	int mod1;
	int* p;
	p = &mod1;
	printf("Za modele od A10 do A41s izaberite opciju 1 \n");
	printf("Za modele od A41 do A80 izaberite opciju 2 \n");
	printf("Za modele od S6 do S9 izaberite opciju 3 \n");
	printf("Za modele od S9+ do S20+ izaberite opciju 4 \n");
	printf("Za modele od J5 do J7 izaberite opciju 5 \n");
	do {
		scanf("%d", &mod1);
	} while (mod1 < 1 || mod1>5);
	if (*p == 1) {
		fprintf(fp, " %s ", n1);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 600 || cijena>1200);
    fprintf(fp, " %d \n", cijena);
	}

	if (*p == 2) {
		fprintf(fp, " %s ", n2);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 100 || cijena>1700);
	fprintf(fp, " %d\n ", cijena);
	}

	if (*p == 3) {
		fprintf(fp, " %s ", n3);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 350 || cijena>1150);
	fprintf(fp, " %d\n ", cijena);
	}

	if (*p == 4) {
		fprintf(fp, " %s ", n4);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 2000 || cijena>4500);
	fprintf(fp, " %d\n ", cijena);
	}
	if (*p == 5) {
		fprintf(fp, " %s ", n5);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 200 || cijena>600);
	fprintf(fp, " %d\n ", cijena);
	}
	return 0;
}

int model2(FILE* fp) {
	char ip1[] = "iPhone4 - iPhone7";
	char ip2[] = "iPhone 7 plus - iPhone 8 plus ";
	char ip3[] = " iPhone X , iPhone XR , iPhone XS max ";
	char ip4[] = " iPhone 11, iPhone 11 pro  i iPhone 11 pro max";
	char ip5[] = "iPhone 12,iPhone 12 mini,iPhone 12 pro i iPhone 12 pro max ";
	int mod2;
	printf("Za modele od iPhone4 do iPhone 7 izaberite opciju 1 \n");
	printf("Za modele od iPhone 7 plus do iPhone 8+ izaberite opciju 2 \n");
	printf("Za modele  iPhone X , iPhone XR , iPhone XS max izaberite opciju 3 \n");
	printf("Za modele iPhone 11, iPhone 11 pro  i iPhone 11 pro max izaberite opciju 4 \n");
	printf("Za modele iPhone 12,iPhone 12 mini,iPhone 12 pro i iPhone 12 pro max izaberite opciju 5 \n");
	do {
		scanf("%d", &mod2);
	} while (mod2 < 1 || mod2>5);
	if (mod2 == 1) {
		fprintf(fp, " %s ", ip1);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 400 || cijena>1200);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod2 == 2) {
		fprintf(fp, " %s ", ip2);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
		} while (cijena < 700 || cijena>1600);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod2 == 3) {
		fprintf(fp, " %s ", ip3);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 1400 || cijena>2500);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod2 == 4) {
		fprintf(fp, " %s ", ip4);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 2000 || cijena>3800);
		fprintf(fp, " %d\n ", cijena);
	}
	if (mod2 == 5) {
		fprintf(fp, " %s ", ip5);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
		} while (cijena < 3000 || cijena>5000);
		fprintf(fp, " %d\n ", cijena);
	}
	return 0;
}

int model3(FILE* fp) {
	char h1[] = "Huawei y i ascend line";
	char h2[] = "Huawei mate 7 do mate 10";
	char h3[] = "Huawei mate 10 pro do huawei mate 30 pro";
	char h4[] = "Huawei g line";
	char h5[] = "Huawei nova, nexus  line";
	char h6[] = "Huawei P8 lite do huawei P10 pro";
	char h7[] = "Huawei P20 lite do huawei P30 pro";
	int mod3;
	printf("Za modele y i ascend linije izaberite opciju 1 \n");
	printf("Za modele od mate 7 do mate 10 izaberite opciju 2 \n");
	printf("Za modele mate 10 pro do mate 30 pro izaberite opciju 3 \n");
	printf("Za modele g linije izaberite opciju 4 \n");
	printf("Za modele nova, nexus  linija izaberite opciju 5 \n");
	printf("Za modele od P8 lite do P10 pro izaberite opciju 6 \n");
	printf("Za modele od P20 lite do P30 pro izaberite opciju 7 \n");

	do {
		scanf("%d", &mod3);
	} while (mod3 < 1 || mod3>7);
	if (mod3 == 1) {
		fprintf(fp, " %s ", h1);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 400 || cijena>800);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod3 == 2) {
		fprintf(fp, " %s ", h2);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
		} while (cijena < 450 || cijena>1200);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod3 == 3) {
		fprintf(fp, " %s ", h3);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 1700 || cijena>3600);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod3 == 4) {
		fprintf(fp, " %s ", h4);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 200|| cijena>600);
		fprintf(fp, " %d\n ", cijena);
	}
	if (mod3 == 5) {
		fprintf(fp, " %s ", h5);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 350 || cijena>825);
		fprintf(fp, " %d\n ", cijena);
	}
	if (mod3 == 6) {
		fprintf(fp, " %s ", h6);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 350 || cijena>1355);
		fprintf(fp, " %d\n ", cijena);
	}
	if (mod3 == 7) {
		fprintf(fp, " %s ", h7);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 1500 || cijena>3250);
		fprintf(fp, " %d\n ", cijena);
	}
	return 0;
}

int model4(FILE* fp) {
	char x1[] = "MI6-MI8 pro ";
	char x2[] = "MI9 lite do MI9X ";
	char x3[] = " MI10 do MI11 Ultra ";
	char x4[] = " MI Note do MI Note 3 ";
	char x5[] = "MI Note 10 linija ";
	char x6[] = "MI mix do MI mix 3 ";
	char x7[] = "MI max i MI A linija ";
	char x8[] = "Redmi serija ";
	char x9[] = "Redmi note 5 do Redmi Note 8T";
	char x10[] = "Redmi note 9 pa nadalje";

	int mod4;
	printf("Za modele od MI6 do MI8 pro izaberite opciju 1 \n");
	printf("Za modele od MI9 lite do MI9X izaberite opciju 2 \n");
	printf("Za modele od MI10 do MI11 Ultra izaberite opciju 3 \n");
	printf("Za modele od MI Note do MI Note 3 izaberite opciju 4 \n");
	printf("Za modele MI Note 10 linije izaberite opciju 5 \n");;
	printf("Za modele MI mix do MI mix 3 izaberite opciju 6 \n");
	printf("Za modele MI max i MI A linija izaberite opciju 7 \n");
	printf("Za modele Redmi serije izaberite opciju 8 \n");
	printf("Za modele Redmi note 5 do Redmi Note 8T izaberite opciju 9 \n");
	printf("Za ostale Redmi note modele izaberite opciju 10 \n");

	do {
		scanf("%d", &mod4);
	} while (mod4 < 1 || mod4>11);
	if (mod4 == 1) {
		fprintf(fp, " %s ", x1);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
		
		} while (cijena < 400 || cijena>800);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 2) {
		fprintf(fp, " %s ", x2);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 600 || cijena>1000);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 3) {
		fprintf(fp, " %s ", x3);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 1400 || cijena>2650);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 4) {
		fprintf(fp, " %s ", x4);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 300 || cijena>760);
		fprintf(fp, " %d\n ", cijena);
	}
	if (mod4 == 5) {
		fprintf(fp, " %s ", x5);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
		
		} while (cijena < 1200 || cijena>2600);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 6) {
		fprintf(fp, " %s ", x6);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 250 || cijena>680);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 7) {
		fprintf(fp, " %s ", x7);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 250 || cijena>890);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 8) {
		fprintf(fp, " %s ", x8);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
		
		} while (cijena < 500 || cijena>1600);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 9) {
		fprintf(fp, " %s ", x9);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 450 || cijena>1150);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod4 == 10) {
		fprintf(fp, " %s ", x10);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 650 || cijena>2600);
		fprintf(fp, " %d\n ", cijena);
	}
	return 0;
}

int model5(FILE* fp) {
	char g1[] = "Pixel, Pixel XL, Pixel 2 XL ";
	char g2[] = " Pixel 3 ";
	char g3[] = "Pixel 4 ";
	char g4[] = "Pixel 5";
	int mod5;
	printf("Za modele Pixel, Pixel XL, Pixel 2 XL izaberite opciju 1 \n");
	printf("Za modele Pixel 3 serije izaberite opciju 2 \n");
	printf("Za modele Pixel 4 serije izaberite opciju 3 \n");
	printf("Za model Pixel 5 izaberite opciju 4 \n");
	do {
		scanf("%d", &mod5);
	} while (mod5 < 1 || mod5>4);
	if (mod5 == 1) {
		fprintf(fp, " %s ", g1);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 600 || cijena>1000);
		fprintf(fp, " %d\n ", cijena);
	}

	if(mod5 == 2) {
		fprintf(fp, " %s ", g2);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 700 || cijena>1350);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod5 == 3) {
		fprintf(fp, " %s ", g3);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 1350 || cijena>2200);
		fprintf(fp, " %d\n ", cijena);
	}

	if (mod5 == 4) {
		fprintf(fp, " %s ", g4);
		printf("Unesite opis kvara: ");
		getchar();
		fgets(kvar, 150, stdin);
		fprintf(fp, " %s ", kvar);

		do {
			printf("\nUnesite cijenu popravka: ");
			scanf("%d", &cijena);
			
		} while (cijena < 2000 || cijena>4500);
		fprintf(fp, " %d\n ", cijena);
	}
	return 0;
}
